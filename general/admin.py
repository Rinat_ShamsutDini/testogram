from django.contrib import admin
from django.contrib.auth.models import Group
from rangefilter.filters import DateRangeFilter
from general.filters import AuthorFilter, PostFilter
from django_admin_listfilter_dropdown.filters import ChoiceDropdownFilter
from general.models import (
    User,
    Post,
    Comment,
    Reaction
)

@admin.register(User)
class UserModelAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "first_name",
        "last_name",
        "username",
        "email",
        "is_staff",
        "is_superuser",
        "is_active",
        "date_joined",
    )
    list_filter = (
    "is_staff",
    "is_superuser",
    "is_active",
    ("date_joined", DateRangeFilter),
    )
    fieldsets = (
        (
            "Личные данные", {
                "fields": (
                    "first_name",
                    "last_name",
                    "email",
                )
            }
        ),
        (
            "Учетные данные", {
                "fields": (
                    "username",
                    "password",
                )
            }
        ),
        (
            "Статусы", {
                "classes": (
                    "collapse",
                ),
                "fields": (
                    "is_staff",
                    "is_superuser",
                    "is_active",
                )
            }
        ),
        (
            None, {
                "fields": (
                    "friends",
                )
            }
        ),
        (
            "Даты", {
                "fields": (
                    "date_joined",
                    "last_login",
                )
            }
        )

    )
    readonly_fields = (
    "date_joined",
    "last_login",
    )
    # search_fields = (
    # "id",
    # "username",
    # "email",
    # )
@admin.register(Post)
class PostModelAdmin(admin.ModelAdmin):
    list_display = (
       "id",
        "author",
        "title",
        "get_body",
        "created_at",
        "get_comment_count"
    )
    list_display_links = ("author","id")
    readonly_fields = (
        "created_at",
    )
    list_filter = (
    AuthorFilter,
    ("created_at", DateRangeFilter),
    )
    
    def get_comment_count(self, obj):
        return obj.comments.count()

    def get_body(self, obj):
        max_length = 64
        if len(obj.body) > max_length:
            return obj.body[:61] + "..."
        return obj.body

    get_body.short_description = "body"
    get_comment_count.short_description = "comment count"
    
    search_fields = (
    "id",
    "title",    
    )
    
@admin.register(Comment)
class CommentModelAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "author",
        "post",
        "body",
        "created_at",
    )
    search_fields = (    
    "post__title",
    "author__username",
    )
    list_filter = (
        AuthorFilter,
        PostFilter
    )
@admin.register(Reaction)
class ReactionModelAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "author",
        "post",
        "value",
    )
    list_filter = (
        AuthorFilter,
        PostFilter,
        ("value", ChoiceDropdownFilter), 
    )

admin.site.unregister(Group)
